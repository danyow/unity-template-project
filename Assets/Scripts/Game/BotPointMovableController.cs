﻿using OpachaTemplate.Core;
using OpachaTemplate.Core.Units;
using UnityEngine;

namespace OpachaTemplate.Game
{
    public class BotPointMovableController : MovableControllerBase
    {
        [SerializeField] private StepMovableBase movable;
        [SerializeField] private PointsMover pointsMover;

        private Vector3 currentDirection;

        public override void InitMovement()
        {
            // ! TODO : make unsub
            pointsMover.OnNextPoint += () => currentDirection = (pointsMover.TargetPosition - movable.Position).normalized;
        }

        public override void UpdateMovement()
        {
            movable.MoveBy(currentDirection * DeltaTime);
            pointsMover.UpdateCheck(movable.Position);
        }
    }
}
