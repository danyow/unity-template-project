﻿using OpachaTemplate.Core;
using OpachaTemplate.Core.Audio;
using UnityEngine;

namespace OpachaTemplate.Game.Audio
{
    public class GameAudioManager : MonoSingleton<GameAudioManager>
    {
        [SerializeField] private AudioManager2D audioManager;
    }
}
