﻿using OpachaTemplate.Core;
using OpachaTemplate.Core.Attributes;
using OpachaTemplate.Core.Management;
using OpachaTemplate.Core.UI;
using System.Collections;
using UnityEngine;

namespace OpachaTemplate.Game.UI
{
    /* Manager, controlling different ui in game. It's specific to concrete game */

    public class GameUIManager : MonoSingleton<GameUIManager>
    {
        [SerializeField] private PanelActivator panelActivator;

        [SerializeField, ActiveObject] private AnimationUIPanel pauseButtonPopup;

        private IEnumerator Start()
        {
            yield return null;
            panelActivator.ActivatePanel(pauseButtonPopup);
        }

        // -- UI Events --

        public void OnPause()
        {
            ApplicationManager.Instance.Pause();
            GamePanelManager.Instance.ShowPausePanel();
        }
    }
}
