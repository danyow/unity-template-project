﻿using OpachaTemplate.Core.Units;
using UnityEngine;

namespace OpachaTemplate.Game
{
    public class BotRandomMovableController : MovableControllerBase
    {
        [SerializeField] private StepMovableBase movable;
        [SerializeField] private float changeDirectionTime;

        private Vector3 currentDirection;

        float changeDirectionTick = 0;

        private void ChangeDirection() => currentDirection = Random.insideUnitCircle.normalized;

        public override void InitMovement()
        {
            ChangeDirection();
        }

        public override void UpdateMovement()
        {
            changeDirectionTick += DeltaTime;
            if (changeDirectionTick >= changeDirectionTime)
            {
                ChangeDirection();
                changeDirectionTick = 0;
            }

            movable.MoveBy(currentDirection * DeltaTime);
        }
    }
}
