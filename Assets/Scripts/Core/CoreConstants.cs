﻿namespace OpachaTemplate.Core
{
    /*
     * TODO:
     *  - добавить полные строки для MenuItem
     *  - сделать константы либо более универсальные, либо убрать те, которые специфичны для игры
     *  - вынести editor-specific константы в отдельный класс
     */ 

    public static class CC
    {
        #region Logging
        public readonly static string GAME_TAG = "GAME";
        #endregion

        #region Colors
        public readonly static string COLOR_RED1 = "e22";
        public readonly static string COLOR_GREEN1 = "2e2";
        public readonly static string COLOR_BLUE1 = "22e";
        #endregion
        
        #region Time
        public readonly static int STOP_SCALE = 0;
        public readonly static int RUN_SCALE = 1;
        #endregion

        #region Scenes
        public readonly static int LOGO_SCENE = 0;
        public readonly static int MAIN_MENU_SCENE = 1;
        public readonly static int GAME_SCENE = 2;
        public readonly static int FINAL_SCENE = 3;
        public readonly static int DEMO_SCENE = 4;
        #endregion

        #region Assets
        public const string GAME_DATA = "Game Data";

        public const string AUDIO = "Audio";

        public const string SOUND_DATA = "Sound Data";
        public const string MUSIC_DATA = "Music Data";
        
        public const string APP_SETTINGS_DATA = "Application Settings Data";
        #endregion

        #region Menus
        public const string TOOLS_MENU = "Opacha Tools";
        
        public const string CLEAR_STORAGE = "Clear storage";
        public const string CLEAR_STORAGE_SHCUT = "%q";
        public const int CLEAR_STORAGE_ORDER = 0;
        
        public const string RESOLUTION_HOR = "Switch resolution horizontal";
        public const string RESOLUTION_HOR_SHCUT = "%&1";
        public const int RESOLUTION_HOR_ORDER = 11;
        
        public const string RESOLUTION_VER = "Switch resolution vertical";
        public const string RESOLUTION_VER_SHCUT = "%&2";
        public const int RESOLUTION_VER_ORDER = 12;
        #endregion

        #region Windows
        public const string TOOLS_WINDOW = "Tools window";
        #endregion

        #region System
        public readonly static string HOR_RESOLUTION_NAME = "Full HD - Landscape (1920x1080)";
        public readonly static string VER_RESOLUTION_NAME = "Full HD - Portrait (1080x1920)";
        #endregion

        #region Help Editor

        public const string PERSISTENT_HELP =
            "Is this behavior available across scenes or exists just in current scene" +
            "and destroyed after scene switching?";

        public const string FOLLOW_HELP =
            "To be followerd, target must implement IMovable interface";

        public const string FOLLOW_SPEED_HELP =
            "To be followerd by speed-dependent, you IMovable should implement Velocity property";

        #endregion

        #region Input
        public readonly static string HORIZONTAL_AXIS = "Horizontal";
        public readonly static string VERTICAL_AXIS = "Vertical";
        #endregion

        #region Animation
        public readonly static string SHOW_PANEL_TRIGGER = "Show";
        public readonly static string HIDE_PANEL_TRIGGER = "Hide";
        public readonly static string SHOW_POPUP_NAME = "ShowPopup";
        public readonly static string HIDE_POPUP_NAME = "HidePopup";
        #endregion
    }
}
