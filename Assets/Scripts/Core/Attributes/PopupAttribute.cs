﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class PopupAttribute : PropertyAttribute
    {
        public string[] Values { get; }

        public PopupAttribute(string[] values)
        {
            Values = values;
        }
    }
}
