﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ButtonAttribute : PropertyAttribute
    {
        public string MethodName { get; }

        public ButtonAttribute(string methodName)
        {
            MethodName = methodName;
        }
    }
}
