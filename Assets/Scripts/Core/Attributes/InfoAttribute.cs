﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Attributes
{
    public enum InfoType
    {
        Info,
        Warning,
        Error,
    }
    
    [AttributeUsage(AttributeTargets.Field)]
    public class InfoAttribute : PropertyAttribute
    {
        public string Message { get; }
        public InfoType InfoType { get; set; }

        public InfoAttribute(string message)
        {
            Message = message;
        }
    }
}
