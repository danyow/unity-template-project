﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class NamedEnumAttribute : PropertyAttribute
    {
        public Type enumType;
        public NamedEnumAttribute (Type type)
        {
            enumType = type;
        }
    }
}
