﻿using UnityEngine;

namespace OpachaTemplate.Core.UI
{
    /* ui element, that is showing with animation via Animation component */

    [RequireComponent(typeof(Animation))]
    public class AnimationUIPanel : UIPanel
    {
        private readonly string showName = CC.SHOW_POPUP_NAME;
        private readonly string hideName = CC.HIDE_POPUP_NAME;

        [SerializeField] private new Animation animation;

        private void Reset() => animation = GetComponent<Animation>();

        public override void Show() => animation.Play(showName);

        public override void Hide() => animation.Play(hideName);
    }
}
