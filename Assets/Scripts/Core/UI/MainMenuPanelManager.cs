﻿using OpachaTemplate.Core.Attributes;
using OpachaTemplate.Core.Management;
using UnityEngine;

namespace OpachaTemplate.Core.UI
{
    public class MainMenuPanelManager : MonoSingleton<MainMenuPanelManager>
    {
        [SerializeField] private PanelActivator panelActivator;

        [SerializeField, ActiveObject] private UIPanel mainMenuPanel;
        [SerializeField, ActiveObject] private UIPanel levelsPanel;
        
        [SerializeField, ActiveObject] private UIPanel gameTitlePopup;
        [SerializeField, ActiveObject] private UIPanel playGameButtonPopup;
        [SerializeField, ActiveObject] private UIPanel demoButtonPopup;
        [SerializeField, ActiveObject] private UIPanel exitButtonPopup;

        public void ShowMainMenuPanel() => panelActivator.ActivatePanel(mainMenuPanel);
        public void HideMainMenuPanel() => panelActivator.DeactivatePanel(mainMenuPanel);
        
        public void ShowLevelsPanel() => panelActivator.ActivatePanel(levelsPanel);
        public void HideLevelsPanel() => panelActivator.DeactivatePanel(levelsPanel);
        
        public void ShowGameTitlePopup() => panelActivator.ActivatePanel(gameTitlePopup);
        public void HideGameTitlePopup() => panelActivator.DeactivatePanel(gameTitlePopup);
        
        public void ShowPlayGameButtonPopup() => panelActivator.ActivatePanel(playGameButtonPopup);
        public void HidePlayGameButtonPopup() => panelActivator.DeactivatePanel(playGameButtonPopup);
        
        public void ShowDemoButtonPopup() => panelActivator.ActivatePanel(demoButtonPopup);
        public void HideDemoButtonPopup() => panelActivator.DeactivatePanel(demoButtonPopup);
        
        public void ShowExitButtonPopup() => panelActivator.ActivatePanel(exitButtonPopup);
        public void HideExitButtonPopup() => panelActivator.DeactivatePanel(exitButtonPopup);

        // -- UI Events --

        public void OnPlay() => ApplicationManager.Instance.StartGame();
        
        public void OnDemo() => ApplicationManager.Instance.OpenDemo();

        public void OnExit() => ApplicationManager.Instance.ExitGame();
    }
}