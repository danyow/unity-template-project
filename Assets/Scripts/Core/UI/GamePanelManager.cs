﻿using UnityEngine;
using OpachaTemplate.Core.Attributes;
using OpachaTemplate.Core.Extensions.UIs;
using OpachaTemplate.Core.Management;

namespace OpachaTemplate.Core.UI
{
    /* Manager, controlling base game panels:
     *  - game panel
     *  - pause panel
     *  - win panel
     *  - lose panel
    */

    public class GamePanelManager : MonoSingleton<GamePanelManager>
    {
        [SerializeField] private PanelActivator panelActivator;

        [SerializeField, ActiveObject] private UIPanel gamePanel;
        [SerializeField, ActiveObject] private UIPanel pausePanel;
        [SerializeField, ActiveObject] private UIPanel winPanel;
        [SerializeField, ActiveObject] private UIPanel losePanel;

        private UIPanel[] AllPanels => new[] { gamePanel, pausePanel, winPanel, losePanel };

        public void ShowGamePanel() => panelActivator.ActivatePanel(gamePanel);
        public void HideGamePanel() => panelActivator.DeactivatePanel(gamePanel);

        public void ShowPausePanel() => panelActivator.ActivatePanel(pausePanel);
        public void HidePausePanel() => panelActivator.DeactivatePanel(pausePanel);

        public void ShowWinPanel() => panelActivator.ActivatePanel(winPanel);
        public void HideWinPanel() => panelActivator.DeactivatePanel(winPanel);

        public void ShowLosePanel() => panelActivator.ActivatePanel(losePanel);
        public void HideLosePanel() => panelActivator.DeactivatePanel(losePanel);

        public void ShowOnlyGame() => panelActivator.ShowOnly(AllPanels, gamePanel);
        public void HideAll() => panelActivator.HideAll(AllPanels);

        // -- UI Events --

        public void OnResume()
        {
            ApplicationManager.Instance.Resume();
            HidePausePanel();
        }
        
        public void OnMenu()
        {
            ApplicationManager.Instance.Resume();
            HidePausePanel();
            ApplicationManager.Instance.OpenMainMenu();
        }
        
        public void OnContinue()
        {
            ApplicationManager.Instance.Resume();
            HideAll();
            ApplicationManager.Instance.NextLevel();
        }
        
        public void OnRestart()
        {
            ApplicationManager.Instance.Resume();
            ApplicationManager.Instance.RestartLevel();
        }
    }
}