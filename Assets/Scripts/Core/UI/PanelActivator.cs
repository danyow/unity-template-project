﻿using UnityEngine;

namespace OpachaTemplate.Core.UI
{
    /* Component controlling showing & hiding UIPanels */

    public class PanelActivator : MonoBehaviour
    {
        public void ActivatePanel(UIPanel panel)
        {
            panel.OnStartShow += OnStartShow;
            panel.Show();
        }

        public void DeactivatePanel(UIPanel panel)
        {
            panel.OnEndHide += OnEndHide;
            panel.Hide();
        }

        private void OnStartShow(UIPanel panel)
        {
            panel.OnStartShow -= OnStartShow;
            panel.gameObject.SetActive(true);
        }

        private void OnEndHide(UIPanel panel)
        {
            panel.OnEndHide -= OnEndHide;
            panel.gameObject.SetActive(false);
        }
    } 
}
