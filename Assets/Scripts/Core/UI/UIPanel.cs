﻿using UnityEngine;
using System;

namespace OpachaTemplate.Core.UI
{
    /* Base class for ui elements, that are to show */
    
    // TODO: add events on end show, on start hide
    public abstract class UIPanel : MonoBehaviour
    {
        public virtual void Show() => StartShow();
        public virtual void Hide() => EndHide();

        public event Action<UIPanel> OnStartShow;
        public event Action<UIPanel> OnEndHide;

        protected void StartShow() => OnStartShow?.Invoke(this);
        protected void EndHide() => OnEndHide?.Invoke(this);

    }
}
