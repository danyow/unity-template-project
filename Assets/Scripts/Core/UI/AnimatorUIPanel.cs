﻿using UnityEngine;

namespace OpachaTemplate.Core.UI
{
    /* ui element, that is showing with animation via Animator component */

    [RequireComponent(typeof(Animator))]
    public class AnimatorUIPanel : UIPanel
    {
        private readonly int showHash = Animator.StringToHash(CC.SHOW_PANEL_TRIGGER);
        private readonly int hideHash = Animator.StringToHash(CC.HIDE_PANEL_TRIGGER);

        [SerializeField] private Animator animator;

        private void Reset() => animator = GetComponent<Animator>();

        public override void Show()
        {
            base.Show();
            animator.SetTrigger(showHash);
        }

        public override void Hide() => animator.SetTrigger(hideHash);
    }
}
