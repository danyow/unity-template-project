﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace OpachaTemplate.Core.UI
{
    [RequireComponent(typeof(Image))]
    public class ImageShifter : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Vector3 pressedOffset;
        
        public void OnPointerDown(PointerEventData eventData) => transform.localPosition += pressedOffset;
        public void OnPointerUp(PointerEventData eventData) => transform.localPosition -= pressedOffset;
    }
}