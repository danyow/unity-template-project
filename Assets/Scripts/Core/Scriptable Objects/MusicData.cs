﻿using UnityEngine;
using OpachaTemplate.Core.Extensions.Randoms;

namespace OpachaTemplate.Core.Audio.Data 
{
    [CreateAssetMenu(menuName = CC.GAME_DATA + "/" + CC.AUDIO + "/" + CC.MUSIC_DATA)]
    public class MusicData : ScriptableObject
    {
        [SerializeField] private AudioClip clip;
        [SerializeField] private Diapason tempoDiapason;
        [SerializeField] private bool isLoop;

        public AudioClip Clip => clip;

        public bool IsLoop => isLoop;

        public Diapason TempoDiapason => tempoDiapason; 
        public float RandomTempo => tempoDiapason.Random();
    }
}
