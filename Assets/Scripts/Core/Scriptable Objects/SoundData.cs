﻿using UnityEngine;
using OpachaTemplate.Core.Extensions.Randoms;

namespace OpachaTemplate.Core.Audio.Data 
{
    [CreateAssetMenu(menuName = CC.GAME_DATA + "/" + CC.AUDIO + "/" + CC.SOUND_DATA)]
    public class SoundData : ScriptableObject
    {
        [SerializeField] private AudioClip clip;
        [SerializeField] private Diapason volumeDiapason;
        [SerializeField] private Diapason pitchDiapason;

        public AudioClip Clip => clip;

        public Diapason VolumeDiapason => volumeDiapason;
        public Diapason PitchDiapason => pitchDiapason;

        public float RandomVolume => volumeDiapason.Random();
        public float RandomPitch => pitchDiapason.Random();
    }
}
