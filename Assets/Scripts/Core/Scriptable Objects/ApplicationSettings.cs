﻿using UnityEngine;

namespace OpachaTemplate.Core
{
    [CreateAssetMenu(menuName = CC.GAME_DATA + "/" + CC.APP_SETTINGS_DATA)]
    public class ApplicationSettings : ScriptableSingleton<ApplicationSettings>
    {
        [SerializeField] public bool CheckSingletonUniqueness = true;
        [SerializeField] public bool ShouldCreateSingletonIfNone = false;
    }
}
