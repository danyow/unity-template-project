﻿using UnityEngine;
using OpachaTemplate.Core.Management;

namespace OpachaTemplate.Core.Logo
{
    public class LogoController : MonoBehaviour
    {
        public void LogoAppeared() => ApplicationManager.Instance.OpenMainMenu();
    }
}