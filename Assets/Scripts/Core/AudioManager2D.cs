﻿using OpachaTemplate.Core.Audio.Data;
using UnityEngine;

namespace OpachaTemplate.Core.Audio
{
    public class AudioManager2D : MonoSingleton<AudioManager2D>, IAudio2D
    {
        [SerializeField] private AudioSource soundsAudioSource;
        [SerializeField] private AudioSource musicAudioSource;

        public void PlaySound(SoundData sound)
        {
            soundsAudioSource.volume = sound.RandomVolume;
            soundsAudioSource.pitch = sound.RandomPitch;

            soundsAudioSource.PlayOneShot(sound.Clip);
        }

        public void PlayMusic(MusicData music)
        {
            musicAudioSource.clip = music.Clip;
            musicAudioSource.loop = music.IsLoop;
            musicAudioSource.pitch = music.RandomTempo;

            musicAudioSource.Play();
        }

        public void StopSound() => soundsAudioSource.Stop();
        public void StopMusic() => musicAudioSource.Stop();
        public void PauseMusic() => musicAudioSource.Pause();
        public void UnpauseMusic() => musicAudioSource.UnPause();

        public void SetMusicVolume(float volume) => musicAudioSource.volume = volume;
    }
}
