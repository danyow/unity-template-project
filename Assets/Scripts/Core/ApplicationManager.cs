﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;

namespace OpachaTemplate.Core.Management
{
    /* TODO:
       - make this class abstract with virtual methods,
         so Game App Manager can override these methods for game-specific needs
     */
    
    public class ApplicationManager : MonoSingleton<ApplicationManager>, IApplication
    {
        [Preserve] // preserve from code stripping
        [SerializeField] private ApplicationSettings applicationSettings; // need ref to scriptable settings so it will not removed from build

        private int CurrentSceneIndex => SceneManager.GetActiveScene().buildIndex;

        private void SetTimeScale(float scale) => Time.timeScale = scale;
        private void OpenScene(int index) => SceneManager.LoadScene(index); // add event on swith scene ?

        public void Pause() => SetTimeScale(CC.STOP_SCALE);
        public void Resume() => SetTimeScale(CC.RUN_SCALE);

        public void RestartLevel() => OpenScene(CurrentSceneIndex);
        public void NextLevel() => OpenScene(CurrentSceneIndex + 1);

        public void OpenMainMenu() => OpenScene(CC.MAIN_MENU_SCENE);
        public void StartGame() => OpenScene(CC.GAME_SCENE);
        public void RestartGame() => OpenScene(CC.LOGO_SCENE);
        public void OpenDemo() => OpenScene(CC.DEMO_SCENE);

        public void ExitGame() => Application.Quit();
    }
}
