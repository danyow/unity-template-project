﻿using System;
using Opacha.Profit.Extensions.Transforms;
using OpachaTemplate.Core.Attributes;
using UnityEngine;

namespace OpachaTemplate.Core
{
    /* Класс, который предоставляет логику, необходимую для продвижения по точкам. 
     * Хранит текущую целевую точку, а также обновляет состояние и оповещает, когда очередная точка пути достигнута
     * 
     * TODO:
     *  + vertical movement
     *  + multipoints movement
     *  - debug draw & editor (horizontal & vertical movement)
     *  - different strategies:
     *    - loop
     *    - inverse
     *    - once
     */

    public class PointsMover : MonoBehaviour
    {
        private const float reachedThreshold = 0.01f;

        [SerializeField] private Transform[] points;
        
        private int targetPointIdx = 1;
        public Vector3 TargetPosition => points[targetPointIdx].position;

        public event Action OnNextPoint;

        private void Start()
        {
            OnNextPoint?.Invoke();
        }

        public void UpdateCheck(Vector3 currentPosition)
        {
            if (HasReachedPoint(currentPosition)) NextPosition();
        }

        // TODO: move to extensions
        private bool HasReachedPoint(Vector3 currentPosition) => (currentPosition - TargetPosition).sqrMagnitude <= reachedThreshold;
        private void NextPosition()
        {
            targetPointIdx++;
            if (targetPointIdx >= points.Length) targetPointIdx = 0;
            OnNextPoint?.Invoke();
        }
        
        [SerializeField, Button(nameof(FindChildrenPoints))] private byte _findButton;
        private void FindChildrenPoints() => points = transform.GetChildren();
    }
}