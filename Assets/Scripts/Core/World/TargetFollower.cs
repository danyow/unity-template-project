﻿using System;
using OpachaTemplate.Core.Attributes;
using UnityEngine;

/*  TODO: 
 *   - Update type (update, late update, fixed) + delta time 
 *   - Rotation offset
 *   - bounds via gizmos
*/
namespace OpachaTemplate.Core.Units
{
    [Serializable]
    public class SpeedDependentSettings
    {
        [Info(CC.FOLLOW_SPEED_HELP, InfoType = InfoType.Warning)]
        [SerializeField] public float minSpeed = 1f;

        [SerializeField, Range(0f, 1f)] private float speedMultiplier = 0.5f;

        public float MinSpeed => minSpeed;
        public float SpeedMultiplier => speedMultiplier;
    }

    public class TargetFollower : MonoBehaviour
    {
        [Header("Target")]
        [SerializeField, Info(CC.FOLLOW_HELP)] private MovableBase target;

        [Header("Settings")]
        [Space]
        [SerializeField] private Vector3 offset;
        [SerializeField] private bool hasBounds;
        [ShowIf(nameof(hasBounds)), SerializeField] private Transform leftTopCorner;
        [ShowIf(nameof(hasBounds)), SerializeField] private Transform rightBottomCorner;

        [Header("Behavior"), Space]
        [Popup(new[]
        {
            nameof(UpdateSimple),
            nameof(UpdateGradual),
            nameof(UpdateSmoothing),
            nameof(UpdateSpeedDependent)
        })]
        [SerializeField]
        private int followType;

        [ShowIf(nameof(followType), 1), SerializeField] private float followSpeed = 1f;

        [ShowIf(nameof(followType), 2), SerializeField] private float smoothTime = 1f;

        [ShowIf(nameof(followType), 3), SerializeField] private SpeedDependentSettings speedDependentSettings;

        private Vector3 targetPosition;
        private Vector3 velocity;
        private Rect boundsRect;

        // Start is called before the first frame update
        void Awake()
        {
            if (hasBounds)
            {
                float width = rightBottomCorner.position.x - leftTopCorner.position.x;
                float height = Mathf.Abs(rightBottomCorner.position.y - leftTopCorner.position.y);
                boundsRect = new Rect(leftTopCorner.position.x, rightBottomCorner.position.y, width, height);
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            targetPosition = target.Position + offset;

            switch (followType)
            {
                case 0: UpdateSimple(); break;
                case 1: UpdateGradual(); break;
                case 2: UpdateSmoothing(); break;
                case 3: UpdateSpeedDependent(); break;
            }

            if (hasBounds) UpdateBounds();
        }

        private void UpdateSimple() => transform.position = targetPosition;
        private void UpdateGradual() => transform.position = Vector3.MoveTowards(transform.position, targetPosition, followSpeed * Time.deltaTime);
        private void UpdateSmoothing() => transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        private void UpdateSpeedDependent()
        {
            float moveSpeed = target.Velocity.magnitude * speedDependentSettings.SpeedMultiplier;
            float speed = Mathf.Clamp(moveSpeed, speedDependentSettings.MinSpeed, Mathf.Infinity);
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }

        private void UpdateBounds()
        {
            float x = Mathf.Clamp(transform.position.x, boundsRect.xMin, boundsRect.xMax);
            float y = Mathf.Clamp(transform.position.y, boundsRect.yMin, boundsRect.yMax);
            float z = transform.position.z;
            transform.position = new Vector3(x, y, z);
        }
    }
}
