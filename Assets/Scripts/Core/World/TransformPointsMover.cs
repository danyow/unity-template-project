﻿using UnityEngine;

namespace OpachaTemplate.Core
{
    public class TransformPointsMover : PointsMover
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private Transform moveTarget;

        private void Update()
        {
            moveTarget.position = Vector3.MoveTowards(moveTarget.position, TargetPosition, moveSpeed * Time.deltaTime);
            UpdateCheck(moveTarget.position);
        }
    }
}