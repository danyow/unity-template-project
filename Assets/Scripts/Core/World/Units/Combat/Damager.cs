﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Combat
{
    public class Damager : MonoBehaviour, IDamager
    {
        [SerializeField] private float damage;


        public event Action<float> OnDidDamage;

        // TODO: check 0 damage
        public void DoDamage(IHealth health)
        {
            health.UpdateHealth(-damage);
            OnDidDamage?.Invoke(damage);
        }
    }
}