﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Combat
{
    public class Health : MonoBehaviour, IHealth
    {
        [SerializeField] private float healthMax;

        public float HealthAmount { get; private set; }
        public float MaxHealthAmount => healthMax;

        public event Action OnHealthUpdated;
        public event Action OnDied;

        private void Awake()
        {
            HealthAmount = healthMax;
        }

        public void UpdateHealth(float amount)
        {
            HealthAmount += amount;
            HealthAmount = Mathf.Clamp(HealthAmount, 0, healthMax);

            HealthAmount.Log("health");
            OnHealthUpdated?.Invoke();
            if (HealthAmount <= 0)
            {
                OnDied?.Invoke();
            }
        }
    }
}