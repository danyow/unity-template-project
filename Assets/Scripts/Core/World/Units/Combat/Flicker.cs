﻿using System.Collections;
using UnityEngine;

namespace OpachaTemplate.Core.Combat
{
    public class Flicker : MonoBehaviour
    {
        [SerializeField] private Renderer targetRenderer;

        [SerializeField] private Color flickerColor;
        [SerializeField] private float flickerSpeed;

        private Color initColor;

        private void Awake()
        {
            initColor = targetRenderer.material.color;
        }

        public void DoFlicker()
        {
            StopAllCoroutines();
            StartCoroutine(FlickerProcess());
        }

        private IEnumerator FlickerProcess()
        {
            targetRenderer.material.color = flickerColor;
            Color.RGBToHSV(targetRenderer.material.color, out float ch, out float cs, out float cv);
            while (cs > 0)
            {
                cs -= flickerSpeed * Time.deltaTime;
                targetRenderer.material.color = Color.HSVToRGB(ch, cs, cv);
                yield return null;
            }
            targetRenderer.material.color = initColor;
        }
    }
}