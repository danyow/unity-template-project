﻿using UnityEngine;

namespace OpachaTemplate.Core.Combat
{
    public class DeathHandler : MonoBehaviour
    {
        [SerializeField] private Health health;
        [SerializeField] private Behaviour[] componentsToDisable;
        [SerializeField] private GameObject[] objectsToDisable;

        private void OnEnable()
        {
            health.OnDied += OnDied;
        }
        
        private void OnDisable()
        {
            health.OnDied -= OnDied;
        }

        private void OnDied()
        {
            name.Log("Died");
            foreach (var comp in componentsToDisable)
            {
                comp.enabled = false;
            }
            foreach (var o in objectsToDisable)
            {
                o.SetActive(false);
            }
        }
    }
}
