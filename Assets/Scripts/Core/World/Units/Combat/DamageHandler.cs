﻿using UnityEngine;

namespace OpachaTemplate.Core.Combat
{
    public class DamageHandler : MonoBehaviour
    {
        [SerializeField] private Health health;
        [SerializeField] private Flicker flicker;

        private void OnEnable()
        {
            health.OnHealthUpdated += OnHealthUpdated;
        }
        
        private void OnDisable()
        {
            health.OnHealthUpdated -= OnHealthUpdated;
        }

        private void OnHealthUpdated()
        {
            health.HealthAmount.Log("Current health");
            flicker.DoFlicker();
        }
    }
}
