﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public class PlayerStepMovableTopController : MovableControllerBase
    {
        [SerializeField] private StepMovableBase movable;

        public override void UpdateMovement()
        {
            float hor = Input.GetAxis(CC.HORIZONTAL_AXIS);
            float ver = Input.GetAxis(CC.VERTICAL_AXIS);
            Vector3 moveDir = new Vector3(hor, ver, 0) * DeltaTime;

            movable.MoveBy(moveDir);
        }
    }
}
