﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public class PlayerJumpableSideController : MovableControllerBase
    {
        [SerializeField] private ForceMovableBase movable;

        public override void UpdateMovement()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                movable.MoveWith(Vector3.up);
            }
        }
    }
}
