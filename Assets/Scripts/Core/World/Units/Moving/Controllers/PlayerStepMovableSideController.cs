﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public class PlayerStepMovableSideController : MovableControllerBase
    {
        [SerializeField] private StepMovableBase movable;

        public override void UpdateMovement()
        {
            float hor = Input.GetAxis(CC.HORIZONTAL_AXIS);
            Vector3 moveDir = new Vector3(hor, 0, 0) * DeltaTime;
            movable.MoveBy(moveDir);
        }
    }
}
