﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public abstract class MovableControllerBase : MonoBehaviour, IMovableController
    {
        enum UpdateType
        {
            Update,
            FixedUpdate
        }

        [SerializeField] private UpdateType updateType;

        protected float DeltaTime => updateType == UpdateType.Update ? Time.deltaTime : Time.fixedDeltaTime;

        private void Awake()
        {
            InitMovement();
        }
        
        // replace on subscribe
        void Update()
        {
            if (updateType == UpdateType.Update) UpdateMovement();
        }
        
        // replace on subscribe
        private void FixedUpdate()
        {
            if (updateType == UpdateType.FixedUpdate) UpdateMovement();
        }

        public virtual void InitMovement() { }
        public abstract void UpdateMovement();
    }
}
