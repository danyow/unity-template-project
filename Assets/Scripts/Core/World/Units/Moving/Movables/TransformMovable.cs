﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public class TransformMovable : StepMovableBase
    {
        // do custom implement
        public override Vector3 Velocity => throw new System.NotSupportedException();

        public override Vector3 Position => transform.position;

        public override void MoveBy(Vector3 step) => transform.position += step * moveSpeed;
    }
}
