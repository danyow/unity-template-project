﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    /* TODO: add mass independency ?
     */

    public class Rigidbody2dMovable : StepMovableBase 
    {
        [SerializeField] private Rigidbody2D rigidbody2d;

        // TODO: custom calc velocity
        public override Vector3 Velocity => rigidbody2d.velocity;

        public override Vector3 Position => rigidbody2d.position;

        public override void MoveBy(Vector3 step)
        {
            switch (rigidbody2d.bodyType)
            {
                case RigidbodyType2D.Dynamic: MoveDynamic(step); break;
                case RigidbodyType2D.Kinematic: MoveKinematic(step); break;
            }
        }

        private void MoveDynamic(Vector3 moveStep) => rigidbody2d.AddForce(moveStep * moveSpeed);
        
        private void MoveKinematic(Vector3 moveStep) => rigidbody2d.MovePosition((Vector3)rigidbody2d.position + moveStep * moveSpeed);
    }
}
