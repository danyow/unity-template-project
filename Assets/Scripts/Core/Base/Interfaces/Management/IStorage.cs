﻿namespace OpachaTemplate.Core
{
    public interface IStorage
    {
        void Save(string key, float value);
        void Save(string key, int value);
        void Save(string key, string value);

        float LoadFloat(string key);
        int LoadInt(string key);
        string LoadString(string key);

        void ClearAll();
    }
}