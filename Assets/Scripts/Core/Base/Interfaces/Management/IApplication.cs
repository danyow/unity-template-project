﻿namespace OpachaTemplate.Core
{
    public interface IApplication
    {
        void StartGame();
        void ExitGame();
        
        void OpenMainMenu();
        void NextLevel();
        void RestartGame();
        void RestartLevel();
        
        void Pause();
        void Resume();
    }
}