﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public interface IForceMovable : IMovable
    {
        void MoveWith(Vector3 direction);
    }
}
