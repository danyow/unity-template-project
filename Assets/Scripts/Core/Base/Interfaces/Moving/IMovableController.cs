﻿namespace OpachaTemplate.Core.Units
{
    public interface IMovableController
    {
        void InitMovement();
        void UpdateMovement();
    }
}
