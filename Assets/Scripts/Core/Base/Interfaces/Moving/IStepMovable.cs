﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public interface IStepMovable : IMovable
    {
        void MoveBy(Vector3 step);
    }
}
