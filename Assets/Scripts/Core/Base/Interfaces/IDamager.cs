﻿using System;

namespace OpachaTemplate.Core.Combat
{
    public interface IDamager
    {
        void DoDamage(IHealth health);

        event Action<float> OnDidDamage;
    }
}