﻿using UnityEngine;

namespace OpachaTemplate.Core
{
    public interface IRootReference
    {
        GameObject RootObject { get; }
    }
}
