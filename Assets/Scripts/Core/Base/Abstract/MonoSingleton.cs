﻿using OpachaTemplate.Core.Attributes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OpachaTemplate.Core
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [Info(CC.PERSISTENT_HELP), Boxed, SerializeField] private bool isPersistent;
        
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    if (ApplicationSettings.Instance.CheckSingletonUniqueness)
                        _instance = EnsureUniqueness(FindObjectsOfType<T>());
                    else
                        _instance = FindObjectOfType<T>();

                    if (_instance == null && ApplicationSettings.Instance.ShouldCreateSingletonIfNone)
                        _instance = CreateInstance();
                }

                return _instance;
            }
        }

        private void Awake()
        {
            if (ApplicationSettings.Instance.CheckSingletonUniqueness)
                SceneManager.sceneLoaded += OnSceneLoaded;

            if (isPersistent)
            {
                transform.parent = null;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void OnDestroy()
        {
            if (ApplicationSettings.Instance.CheckSingletonUniqueness)
                SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode mode)
        {
            EnsureUniqueness();
        }

        private static T CreateInstance()
        {
            GameObject instanceObject = new GameObject(typeof(T).Name);
            return instanceObject.AddComponent<T>();
        }

        private static T EnsureUniqueness(T[] objects)
        {
            if (objects.Length > 0)
            {
                if (objects.Length > 1)
                {
                    for (int i = objects.Length - 1; i > 0; i--)
                    {
                        Destroy(objects[i].gameObject);
                    }
                }
                
                return objects[0];
            }

            return null;
        }

        private void EnsureUniqueness()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
