﻿using UnityEditor;
using UnityEngine;

namespace OpachaTemplate.Core.Editor.Menus
{
    public static class StorageClearer
    {
        [MenuItem(CC.TOOLS_MENU + "/" + CC.CLEAR_STORAGE + " " + CC.CLEAR_STORAGE_SHCUT, false, CC.CLEAR_STORAGE_ORDER)]
        public static void ClearStorage()
        {
            PlayerPrefs.DeleteAll();
            "Storage cleared!".Log(); // TODO: сделать такие сообщения другим цветом
        }
    }
}
