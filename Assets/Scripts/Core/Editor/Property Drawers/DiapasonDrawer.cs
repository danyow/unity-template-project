﻿using UnityEngine;
using UnityEditor;

namespace OpachaTemplate.Core.Editor.Properties
{
    /* TODO:
     *  - значения по умолчанию
     *  - проверка на корректность (max > min, ..)
     */
     
    [CustomPropertyDrawer(typeof(Diapason), true)]
    public class DiapasonDrawer : PropertyDrawer
    {
        private const int labelWidth = 25;
        private const float fieldWidthMultiplier = 0.5f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var minProperty = property.FindPropertyRelative("minValue");
            var maxProperty = property.FindPropertyRelative("maxValue");

            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var initLabelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = labelWidth;

            Rect valueRect = new Rect(position) { width = position.width * fieldWidthMultiplier };
            EditorGUI.PropertyField(valueRect, minProperty, new GUIContent("Min"));

            valueRect.x = valueRect.xMax;
            EditorGUI.PropertyField(valueRect, maxProperty, new GUIContent("Max"));

            EditorGUIUtility.labelWidth = initLabelWidth;
            EditorGUI.EndProperty();
        }
    }
}
