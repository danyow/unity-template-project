﻿using OpachaTemplate.Core.Attributes;
using UnityEngine;
using UnityEditor;

namespace OpachaTemplate.Core.Editor.Properties
{
    [CustomPropertyDrawer(typeof(InfoAttribute), true)]
    public class InfoDrawer : DecoratorDrawer
    {
        private const float boxHeight = 50;
        
        private InfoAttribute InfoAttribute => (InfoAttribute) attribute;

        private MessageType GetMessageType(InfoType infoType)
        {
            switch (infoType)
            {
                case InfoType.Info: return MessageType.Info;
                case InfoType.Warning: return MessageType.Warning;
                case InfoType.Error: return MessageType.Error;
                default: return MessageType.None;
            };
        }
        
        public override void OnGUI(Rect position)
        {
            position.height = boxHeight;
            EditorGUI.HelpBox(position, InfoAttribute.Message, GetMessageType(InfoAttribute.InfoType));
        }

        public override float GetHeight() => base.GetHeight() + boxHeight;
    }
}
