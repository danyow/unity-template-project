﻿using UnityEngine;
using UnityEditor;
using OpachaTemplate.Core.Attributes;

namespace OpachaTemplate.Core.Editor.Properties
{
    [CustomPropertyDrawer(typeof(BoxedAttribute), true)]
    public class BoxedDrawer : PropertyDrawer
    {
        private const float gap = 5;
        private readonly Color boxedColor = new Color(0.8f, 0.89f, 0.89f);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var rectPos = position;
            rectPos.xMin -= gap;
            rectPos.xMax += gap;

            position.y += gap;
            position.yMax -= gap * 2;
            
            EditorGUI.DrawRect(rectPos, boxedColor);
            EditorGUI.PropertyField(position, property, label);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => base.GetPropertyHeight(property, label) + gap * 2;
    }
}
