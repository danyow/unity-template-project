﻿using OpachaTemplate.Core.Attributes;
using UnityEngine;
using UnityEditor;

namespace OpachaTemplate.Core.Editor.Properties
{
    [CustomPropertyDrawer(typeof(PopupAttribute), true)]
    public class PopupDrawer : PropertyDrawer
    {
        private PopupAttribute PopupAttribute => (PopupAttribute) attribute;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            property.intValue = EditorGUI.Popup(position, property.displayName, property.intValue, PopupAttribute.Values);
        }
    }
}
