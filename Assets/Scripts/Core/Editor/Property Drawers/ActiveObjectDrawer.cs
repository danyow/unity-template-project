﻿using UnityEngine;
using UnityEditor;
using OpachaTemplate.Core.Attributes;

namespace OpachaTemplate.Core.Editor.Properties
{
    [CustomPropertyDrawer(typeof(ActiveObjectAttribute), true)]
    public class ActiveObjectDrawer : PropertyDrawer
    {
        bool isActive = true;
        Rect propRect;
        GameObject refObject;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            GameObject GetReferencedObject(Object prop)
            {
                if (property.objectReferenceValue is MonoBehaviour mono) return mono?.gameObject;
                else if (property.objectReferenceValue is GameObject go) return go;
                return null;
            }

            refObject = GetReferencedObject(property.objectReferenceValue);

            propRect = position;
            propRect.width = 15;
            isActive = EditorGUI.Toggle(propRect, refObject?.activeSelf ?? false);

            refObject?.SetActive(isActive);

            propRect = position;
            propRect.x = position.x + 15;
            propRect.width = position.width - 15;
            EditorGUI.PropertyField(propRect, property);

            EditorGUI.EndProperty();
        }
    }
}
