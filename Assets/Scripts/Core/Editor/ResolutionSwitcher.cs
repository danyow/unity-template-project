﻿using UnityEditor;
using System.Reflection;
using System;
using UEditor = UnityEditor.Editor;

namespace OpachaTemplate.Core.Editor.Menus
{
    public static class ResolutionSwitcher
    {
        #region Private
        private static object gameViewSizes;
        private static MethodInfo getGroup;

        static ResolutionSwitcher()
        {
            var sizesType = typeof(UEditor).Assembly.GetType("UnityEditor.GameViewSizes");
            var singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
            var instanceProp = singleType.GetProperty("instance");

            getGroup = sizesType.GetMethod("GetGroup");
            gameViewSizes = instanceProp.GetValue(null, null);
        }

        private static string[] GetWindowResolutions()
        {
            var group = GetGroup();
            var method = group.GetType().GetMethod("GetDisplayTexts");
            return method.Invoke(group, null) as string[];
        }

        private static object GetGroup() => getGroup.Invoke(gameViewSizes, new object[] { (int)GameViewSizeGroupType.Standalone });

        private static int GetIndex(string[] sizes, string name) => Array.FindIndex(sizes, s => s.Equals(name));

        private static void SetSizeIndexProperty(int index)
        {
            var gameViewType = typeof(UEditor).Assembly.GetType("UnityEditor.GameView");
            var gameView = EditorWindow.GetWindow(gameViewType);

            var sizeSelectionCallback = gameViewType.GetMethod("SizeSelectionCallback", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            sizeSelectionCallback.Invoke(gameView, new object[] { index, null });
        }

        private static void SetWindowResolution(string sizeName)
        {
            string[] sizesNames = GetWindowResolutions();
            int index = GetIndex(sizesNames, sizeName);
            SetSizeIndexProperty(index);
        }
        #endregion

        [MenuItem(CC.TOOLS_MENU + "/" + CC.RESOLUTION_HOR + " " + CC.RESOLUTION_HOR_SHCUT, false, CC.RESOLUTION_HOR_ORDER)]
        public static void SwitchHorizontal()
        {
            SetWindowResolution(CC.HOR_RESOLUTION_NAME);
        }

        [MenuItem(CC.TOOLS_MENU + "/" + CC.RESOLUTION_VER + " " + CC.RESOLUTION_VER_SHCUT, false, CC.RESOLUTION_VER_ORDER)]
        public static void SwitchVertical()
        {
            SetWindowResolution(CC.VER_RESOLUTION_NAME);
        }
    }
}
