using OpachaTemplate.Core.Combat;
using OpachaTemplate.Core.Management;
using OpachaTemplate.Core.UI;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class LoseManager : MonoBehaviour
    {
        [SerializeField] private Health playerHealth;

        private void OnEnable() => playerHealth.OnDied += OnPlayerDied;

        private void OnDisable() => playerHealth.OnDied -= OnPlayerDied;

        private void OnPlayerDied()
        {
            ApplicationManager.Instance.Pause();
            GamePanelManager.Instance.ShowLosePanel();
        }
    }
}