﻿using OpachaTemplate.Core;
using OpachaTemplate.Core.Attributes;
using OpachaTemplate.Core.Management;
using OpachaTemplate.Core.UI;
using System.Collections;
using Opacha.Profit.Extensions.Coroutines;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class DemoUIManager : MonoSingleton<DemoUIManager>
    {
        [SerializeField] private PanelActivator panelActivator;

        [SerializeField, ActiveObject] private UIPanel pauseButtonPopup;
        [SerializeField, ActiveObject] private UIPanel infoPopup;

        private IEnumerator Start()
        {
            yield return null;
            panelActivator.ActivatePanel(pauseButtonPopup);

            this.DelayedAction(() =>
            {
                ApplicationManager.Instance.Pause();
                panelActivator.ActivatePanel(infoPopup);
            }, 1f);
        }

        // -- UI Events --

        public void OnPause()
        {
            ApplicationManager.Instance.Pause();
            GamePanelManager.Instance.ShowPausePanel();
        }

        public void OnCloseInfo()
        {
            ApplicationManager.Instance.Resume();
            panelActivator.DeactivatePanel(infoPopup);
        }
    }
}
