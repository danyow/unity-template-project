using OpachaTemplate.Core.Combat;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class DamageSoundHandler : MonoBehaviour
    {
        [SerializeField] private Health playerHealth;

        private void OnEnable() => playerHealth.OnHealthUpdated += OnHealthUpdated;
        private void OnDisable() => playerHealth.OnHealthUpdated -= OnHealthUpdated;

        private void OnHealthUpdated() => DemoAudioManager.Instance.PlayHit();
    }
}