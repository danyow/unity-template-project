using Opacha.Profit.Extensions.GameObjects;
using Opacha.Profit.Extensions.Randoms;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public abstract class Spawner : MonoBehaviour
    {
        [SerializeField] private GameObject spawnableObject;
        [SerializeField] private bool randomRotation;
        [SerializeField] protected Transform holder;

        protected GameObject Spawn(Vector3 position)
        {
            return spawnableObject.Create(
                position,
                randomRotation ? RandomExtensions.RandomRotationXY() : Quaternion.identity,
                holder);
        }
    }
}