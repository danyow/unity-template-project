using Opacha.Profit.Extensions.Randoms;
using Opacha.Profit.Extensions.Transforms;
using OpachaTemplate.Core.Attributes;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class TreesSpawner : Spawner
    {
        [SerializeField] private float treesCount;
        [SerializeField] private float range;
        
        private void Start()
        {
            SpawnTrees();
        }

        private void SpawnTrees()
        {
            for (int i = 0; i < treesCount; i++)
            {
                Vector3 position = RandomExtensions.RandomPosition(range);
                Spawn(position);
            }
        }

        private void DestroyTrees() => holder.DestroyChildren();

        [Button(nameof(SpawnTrees)), SerializeField, Info(DC.TREE_SPAWNER_HELP)] private byte _spawnButton;
        [Button(nameof(DestroyTrees)), SerializeField] private byte _destroyButton;

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(Vector3.zero, range);
        }
    }
}