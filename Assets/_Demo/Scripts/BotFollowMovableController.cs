﻿using Opacha.Profit.Extensions.Vectors;
using OpachaTemplate.Core.Units;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class BotFollowMovableController : MovableControllerBase
    {
        [SerializeField] private StepMovableBase botMovable;
        
        private MovableBase targetMovable;

        // bad way. Need to replace it when there will be a reference to player in some place (or directly inject)
        public override void InitMovement() => targetMovable = GameObject.FindWithTag(DC.PLAYER_TAG).GetComponent<MovableBase>();

        public override void UpdateMovement()
        {
            Vector3 direction = botMovable.Position.DirectionTo(targetMovable.Position);
            botMovable.MoveBy(direction * DeltaTime);
        }
    }
}
