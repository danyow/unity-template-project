namespace OpachaTemplate.Demo
{
    public class DC
    {
        public const string PLAYER_TAG = "Player";
        public const string PLAYER_POSX = "PlayerXPosition";
        public const string PLAYER_POSY = "PlayerYPosition";
        
        public const string TREE_SPAWNER_HELP =
            "You can try spawning right in editor!";

    }
}