using OpachaTemplate.Core.Management;
using OpachaTemplate.Core.UI;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class WinManager : MonoBehaviour
    {
        [SerializeField] private GameTimeManager gameTimeManager;

        private void OnEnable() => gameTimeManager.TimeEnded += OnTimeEnded;

        private void OnDisable() => gameTimeManager.TimeEnded -= OnTimeEnded;

        private void OnTimeEnded()
        {
            ApplicationManager.Instance.Pause();
            GamePanelManager.Instance.ShowWinPanel();
        }
    }
}