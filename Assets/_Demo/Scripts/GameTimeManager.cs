using System;
using Opacha.Profit.Extensions.Coroutines;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class GameTimeManager : MonoBehaviour
    {
        [SerializeField] private float gameTime;

        public float RemainTime { get; private set; }

        public event Action TimeEnded;
        
        private int actionIdx;
        private void Start()
        {
            RemainTime = gameTime;
            actionIdx = this.RepetitiveAction(() => RemainTime--, 1f);
            this.DelayedAction(EndTime, CoroutineHelper.WaitUntil(() => RemainTime <= 0));
        }

        private void EndTime()
        {
            this.TerminateAction(actionIdx);
            TimeEnded?.Invoke();
        }
    }
}