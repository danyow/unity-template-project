using OpachaTemplate.Core.Management;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class PlayerSaver : MonoBehaviour
    {
        private void OnEnable()
        {
            float x = StorageManager.Instance.LoadFloat(DC.PLAYER_POSX);
            float y = StorageManager.Instance.LoadFloat(DC.PLAYER_POSY);
            
            transform.position = new Vector3(x, y, 0);
        }

        private void OnDisable()
        {
            StorageManager.Instance.Save(DC.PLAYER_POSX, transform.position.x);
            StorageManager.Instance.Save(DC.PLAYER_POSY, transform.position.y);
        }
    }
}