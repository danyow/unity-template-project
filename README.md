# Unity Template Project

ℹ⚠ ***Important: this is a constantly updated document. For now it may not contain full documentation of project. It's better to see the sources directly.*** ⚠ℹ

# General

Unity Template Project is a sample game-project. It contains resources (like scripts, prefabs, scripts) which goal is 
to the goal is to simplify and facilitate development. 

*(Note: as purpose is to make game creating easier and less time consuming,
some implemented concepts may be controversial; anyway an effort was made to make scripts as useful as possible)*

Here are some of features:

 - ready-to-use components
    - moving
	- damaging
	- colliding
	- UI
 - easy app management (app, storage, audio, ...)
 - various types of utils and extensions
 - attributes & drawers
 - demo scene
 
 Roadmap:
 
 - [x] base structure
 - [x] base scenes
 - [x] base management
 - [x] base behaviors
 - [x] camera follow
 - [x] moving behaviors
 - [x] UI & animation
 - [x] combat system
 - [x] base property drawers
 - [x] window & menu tools
 - [x] sprites & materials presets
 - [x] singleton imporiving
 - [x] global refactoring
 - [x] extensions & helpers
 - [x] improving attribute drawers 
 - [x] demo scene
 - [x] improving and refactoring
 - [x] moved utilities to Profit package
 - [_] *readme documentation*
 - [_] processes behavior
 - [_] gizmos components
 - [_] extends base management
 - [_] add effects presets
 - [_] mobile input
 - [_] shooting behavior

# Overview

## Structure

At start, you are given:

 - project structures & folders
 - 4 scenes (**logo, main menu, game, final**)
 - predefind scene hierarchy
 - prefabs for all main things

Game scene hierarchy structure is splitted into 3 global sections:

 - UI
 - World
 - Management
 
![scene-view](https://bitbucket.org/DcrDro/unity-template-project/raw/348534abe52e61784ac27af5361ca08438a29a75/Assets/Documentation~/scene-hierarchy.png) 

## UI
 
Inner structure:

  - UI Camera
  - Panels
     - Game Panel - showing during the game
     - Pause Panel - showing in pause
     - Win Panel - showing after player wins
	 - Lose Panel - showing after player loses
	 
## World
 
In this hierarchy world objects are placed - player; enemies; environment; holders for spawnable objects.
 
Unit prefab has 2 variants - player and enemy. They both has many behavior components attached to them, which are defining the unit behavior.
By hierarchy, each unit prefab contains of **Logic** and **View** sections.

  1. **Logic**: components that independent of any view and exists universally, by themselves

    ![player-root](https://bitbucket.org/DcrDro/unity-template-project/raw/348534abe52e61784ac27af5361ca08438a29a75/Assets/Documentation~/player-components-root.png) 
  
  2. **View**: render and unity specific components, like SpriteRenderer, Rigidbody, Collider. Among of custom components, it contains those which related to default components (i.e. script, which receive colliding events and then re-send them)

    ![player-view](https://bitbucket.org/DcrDro/unity-template-project/raw/348534abe52e61784ac27af5361ca08438a29a75/Assets/Documentation~/player-components-view.png) 
       
## Management
 
General management of the game. By default, it consists of:

  - Application manager
  - Audio manager
  - Storage manager
  - Game manager
